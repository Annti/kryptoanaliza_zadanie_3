# -*- coding: utf-8 -*-
import codecs

LETTERS_MAP = {
    'g': 'a',
    's': 'i',
    'i': 'e',
    'ź': 'o',
    'h': 'z',
    'u': 'n',
    'o': 'c',
    'f': 's',
    'm': 'y',
    'a': 'b',
    'n': 'w',
    't': 'g',
    'ć': 'k',
    'ł': 'd',
    'z': 'l',
    'j': 'ł',
    'ż': 'ż',
    'ó': 'j',
    'ą': 'm',
    'p': 'p',
    'ń': 'r',
    'd': 'h',
    'ś': 'ę',
    'r': 't',
    'l': 'u',
    'q': 'q',
    'e': 'ś',
    'k': 'ć',
    'b': 'f',
    'ę': 'ń',
    'y': 'ó',
    'c': 'ź',
    'x': 'v',
    'w': 'ą',
    'G': 'A',
    'S': 'I',
    'I': 'E',
    'Ź': 'O',
    'H': 'Z',
    'U': 'N',
    'O': 'C',
    'F': 'S',
    'M': 'Y',
    'A': 'B',
    'N': 'W',
    'T': 'G',
    'Ć': 'K',
    'Ł': 'D',
    'Z': 'L',
    'J': 'Ł',
    'Ż': 'Ż',
    'Ó': 'J',
    'Ą': 'M',
    'P': 'P',
    'Ń': 'R',
    'D': 'H',
    'Ś': 'Ę',
    'R': 'T',
    'L': 'U',
    'Q': 'Q',
    'E': 'Ś',
    'K': 'Ć',
    'B': 'F',
    'Ę': 'Ń',
    'Y': 'Ó',
    'C': 'Ź',
    'X': 'V',
    'W': 'Ą'
}

if __name__ == '__main__':

    zrodlo = open('aneta_ochedowska.in').readlines()
    with codecs.open('aneta_ochedowska.out', "w+", "UTF8") as out:
        for line in zrodlo:
            for char in line.decode('utf-8'):
                zmienna = char.encode('utf-8')
                for k in LETTERS_MAP.keys():
                    zmienna = char.encode('utf-8')
                    if zmienna == k:
                        prawda = 1
                        uni = LETTERS_MAP[k].decode('utf-8')
                        break
                    else:
                        prawda = 0
                        uni = char

                out.write(uni)
